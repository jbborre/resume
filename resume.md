---
title: Resume
---

# Jonathan Borre

Software Architect\
2560 S. Sterling Cir.\
East Troy, Wisconsin, 53120\
414-248-1358\
jbborre@gmail.com   

## Work experience

### Watermark, Milwaukee, WI 
*Software Architect (2020 - Present)*
 - Became and advocate for clean architecture
 - Helped to ensure standardization and guidelines for new applications
 - Provided guidance for architectural decisions on new and existing applications
 - Engineered cloud applications leveraging kubernetes, Docker and AWS
 
*Senior Software Engineer (2016 - 2019)*
 - Wrote code for a SaaS product for universities
 - Solution architecture on new projects
 - Worked closely with third party web services for integration into Scopus, Crossref, Orcid, Web of Science
 - Learned Kotlin and often write new code in Kotlin
 - Learned DataStax (graph database) and Activiti (BPMN engine) to leverage on a new product to be sold to universities
 - Upgraded our SaaS product to use latest standards in security (Spring Security and JWT)

### FedEx Services, Brookfield, WI
*Senior Developer Analyst (2009 - 2016)*
 - Awarded 5 Star
 - Wrote and architected the customer entry system for SmartPost (CMI)
 - Dev eloped high transaction Java applications
 - Solution architecture for new and existing applications
 - Worked on highly complex and scalable applications that relied on optimal performance to keep up with tens of millions of events per day
 - Wrote efficient SQL against extremely large datasets
 - Worked closely with business partners to define and fulfill requirements
 - Mentored associate developers
 - Acted as the team lead on the customer facing team

### Business Objects, La Crosse, WI
*Directory Testing Intern (2008 - 2009)*
 - Software testing, Script Writing
 - Wrote test tools in C++ and VB

### Trane Company, La Crosse, WI
*Support Center Analyst (2006 - 2008)*
 - Support/Troubleshoot VPN, Citrix, Networking, PC, and Trane applications

## Education
 - Bachelors of Science, Computer Science, University of Wisconsin – La Crosse (2004-2008)
   - GPA: 3.3
 - Masters of Software Engineering, University of Wisconsin – La Crosse (2007-2011)
   - GPA:3.5

## Additional Skills
 - Develop highly scale-able Java\Kotlin applications using Spring framework and JMS (10+ years)
 - Web development: Spring MVC, React, Javascript, JSF, JSP, HTML, REST, Spring Webflow
 - Experience developing SOAP and REST web services 
 - Database Skills: Postgres, Oracle, PL/SQL, MySQL, Sybase
 - Familiar with C, C++, and C#
 - Often work on projects at home in my free time to learn new technologies or languages
 - 7+ years working in a Scrum Agile environment
 - Familiar with using Oracle Weblogic, Tibco JMS
 - Tools: Intellij, Visual Studio Code, Eclipse, Netbeans, PMD, EMMA, Wiley, Jenkins/Hudson, AppDynamics, Logz.io, Kabana, Graylog
 - Follows software best practices with a passion for clean code and design patterns
 - Maintains code coverage leveraging JUnit, EasyMock, Mockito, and SpringTest
